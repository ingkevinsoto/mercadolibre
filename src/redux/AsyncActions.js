import axios from 'axios';
import * as types from './action-types';
import { first } from 'underscore';

export const fetchProduct = (id) => {
    return (dispactch) => {
        dispactch(fetchProductRequest)
        axios.get(`https://api.mercadolibre.com/items/${id}`)
            .then(response => {
                const dataProduct = (first(response.data, 4));
                dispactch(fetchProductSucces(dataProduct))
            })
            .catch(error => {
                const errorMsg = error.payload
                dispactch(fetchProductFailure(errorMsg))
            })
    }
}


export const fetchSearchProduct = (searchParams) => {
    return (dispactch) => {
        dispactch(fetchProductRequest)
        axios.get(`https://api.mercadolibre.com/sites/MLA/search?q=:${searchParams}`)
            .then(response => {
                const params = (first(response.data.results, 4));
                dispactch(fetchProductSucces(params))
            })
            .catch(error => {
                const errorMsg = error.payload
                dispactch(fetchProductFailure(errorMsg))
            })
    }
}

export const fetchProductSucces = (dataProduct, params) => {
    return {
        type: types.FETCH_PRODUCT_SUCCESS,
        payload: {
            dataProduct,
            params
        }
    }
}

export const fetchProductFailure = (error) => {
    return {
        type: types.FETCH_PRODUCT_FAILURE,
        payload: error
    }
}

export const fetchProductRequest = (error) => {
    return {
        type: types.FETCH_PRODUCT_REQUEST,
        error
    }
}



export default { fetchProduct, fetchProductSucces, fetchProductFailure, fetchProductRequest, fetchSearchProduct }
