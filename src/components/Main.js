import React from 'react';
import { Switch, Route } from "react-router-dom";
import ProductDetail from './ProductDetail';
import SearchResult from './SearchResult';

const Main = () => {
    return (
        <div className="boxMain">
            <Switch>
                <Route exact path="/" component={SearchResult} />
                <Route path='/ProductDetail/:id' component={ProductDetail} />
            </Switch>
        </div>
    )
}

export default Main;