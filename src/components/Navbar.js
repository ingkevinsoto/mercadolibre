import React, { useEffect } from 'react';
import '../style/Navbar.scss';
import Logo_ML from '../assets/Logo_ML.png';
import { connect } from 'react-redux'
import { fetchSearchProduct } from '../redux/AsyncActions';
import PropTypes from "prop-types";


function Navbar({ params, fetchSearchProduct }) {

    let valueInput;
    useEffect(() => {
        fetchSearchProduct()
    }, [])

    return (
        <div className="boxNav">
            <div className="boxImg">
                <img src={Logo_ML} alt="Logo" />
            </div>
            <div className="boxInput">
                <input
                    type="text"
                    placeholder="Nunca dejes de buscar"
                    value={valueInput}
                    ref={node => (valueInput = node)}
                />
                <button
                    disabled={valueInput == ""}
                    type="button"
                    onClick={() => {
                        fetchSearchProduct(valueInput.value)
                        valueInput.value = ""
                    }}
                />
            </div>
        </div>
    )
}

const mapStatetoProps = state => {
    return {
        params: state.params,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchSearchProduct: (searchParams) => dispatch(fetchSearchProduct(searchParams)),
    }
}

Navbar.propTypes = {
    fetchSearchProduct: PropTypes.func,
};



export default connect(
    mapStatetoProps,
    mapDispatchToProps
)(Navbar);
