import React from 'react';
import ic_shipping from '../assets/ic_shipping.png'
import '../style/SearchResult.scss'
import { connect } from 'react-redux';
import { Link } from "react-router-dom";

function SearchResult({ error, loading, params, dataProduct }) {

    if (error) {
        return (
            <div className="container">
                <h6>Hubo un error encontrando los productos</h6>
            </div>
        );
    }
    if (loading) {
        return (
            <div className="container">
                <h6>Cargando...</h6>
            </div>
        );
    }

    return (
        <div className="boxSearch">
            {dataProduct ? dataProduct.map(product => (
                <div className="boxResult" key={product.id}>
                    <div className="boxImg">
                        <img src={product.thumbnail} />
                    </div>
                    <div className="boxCharacteristics">
                        <p className="price">${product.price}</p>
                        <Link className="linkDetail" to={`/ProductDetail/${product.id}`}>
                            <p>{product.title}</p>
                        </Link>
                    </div>
                    <div className="boxShoppingCart">
                        <img src={ic_shipping} />
                    </div>
                    <div className="boxLocation">
                        <p>{product.address.state_name}</p>
                    </div>
                </div>
            )

            ) : dataProduct.length == 0 ? < div><p>No hay Resultados para su busqueda</p></div> : []}
        </div>
    )
}

const mapStatetoProps = state => {
    return {
        params: state.params,
        error: state.error,
        loading: state.loading,
        dataProduct: state.product
    }
}

export default connect(
    mapStatetoProps,
    null
)(SearchResult);
