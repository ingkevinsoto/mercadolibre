import React, { useState, useEffect } from 'react';
import '../style/ProductDetail.scss';


const ProductDetail = (props) => {
    const [dataProduct, setDataProduct] = useState('');
    const [productDescription, setProductDescription] = useState('');

    useEffect(() => {
        fetch(`https://api.mercadolibre.com/items/${props.match.params.id}`)
            .then(response => response.json())
            .then(data => {
                setDataProduct(data)
            });

        fetch(`https://api.mercadolibre.com/items/${props.match.params.id}/description`)
            .then(response => response.json())
            .then(data => {
                setProductDescription(data)
            });
    }, []);

    return (
        <div className="boxDetail">
            <div className="boxImgDetail">
                <img src={dataProduct.thumbnail} />
            </div>
            <div className="boxDetailProduct">
                <p>{dataProduct.condition}-{dataProduct.sold_quantity} vendidos</p>
                <h3>{dataProduct.title}</h3>
                <h1>$ {dataProduct.price}°°</h1>
                <button>comprar</button>
            </div>
            <div className="boxProductDescription">
                <h3>Descripcion del producto</h3>
                <p>{productDescription.plain_text}</p>
            </div>

        </div>
    )
}

export default ProductDetail; 